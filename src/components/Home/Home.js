import React, {useEffect } from 'react';
import styles from './Home.module.css';
import DisplayComponent from '../DisplayComponent/DisplayComponent.lazy';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import {
  setData
} from '../../reducers/reducer';

function Home() {
  
  const dispatch = useDispatch();
  
  useEffect(() => {
    axios.get('https://api.jsonbin.io/b/5ef409df2406353b2e0c4068')
      .then(res => {
        dispatch(setData(res.data.categories));
      })
  });

  return (
    <Container className={styles.Home} data-testid="Home">
      <DisplayComponent />
    </Container>
  )
};

export default Home;
