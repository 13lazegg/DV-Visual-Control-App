import React, { lazy, Suspense } from 'react';

const LazyDisplayComponent = lazy(() => import('./DisplayComponent'));

const DisplayComponent = props => (
  <Suspense fallback={null}>
    <LazyDisplayComponent {...props} />
  </Suspense>
);

export default DisplayComponent;
