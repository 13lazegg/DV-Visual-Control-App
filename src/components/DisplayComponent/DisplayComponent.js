import React, { useRef, useEffect } from 'react';
import styles from './DisplayComponent.module.css';
import videoDefault from '../../assets/default.png';
import { Grid } from '@material-ui/core';
import ControlsComponent from '../ControlsComponent/ControlsComponent.lazy';
import { useSelector, useDispatch } from 'react-redux';
import {
  selectVideo,
  selectedVolume,
  play,
  togglePlay,
  contrast,
  brightness
} from '../../reducers/reducer';

function DisplayComponent() {
  const selected = useSelector(selectVideo);
  const volume = useSelector(selectedVolume);
  const selectedContrast = useSelector(contrast);
  const selectedBrightness = useSelector(brightness);
  const isPlay = useSelector(play);
  const dispatch = useDispatch();
  const vidRef = useRef(null);

  useEffect(() => {
    vidRef.current.volume = volume;
    vidRef.current.style.filter = `brightness(${selectedBrightness}%) contrast(${selectedContrast}%)`;
    if(isPlay){
      vidRef.current.play()
    } else {
      vidRef.current.pause()
    }
  });

  const handleEnd = () => {
    dispatch(togglePlay())
  }

  return (
    <Grid container direction="row" className={styles.DisplayComponent} data-testid="DisplayComponent">
      <Grid item xs={12} className={styles.VideoContainer}>
        <video
          ref={vidRef}
          onEnded={handleEnd}
          poster={selected && selected.thumb ? selected.thumb : videoDefault}
          className={styles.Video}
          src={selected && selected.sources.length ? selected.sources[0] : ''} />
      </Grid>
      <ControlsComponent />
    </Grid>
  )
};

export default DisplayComponent;
