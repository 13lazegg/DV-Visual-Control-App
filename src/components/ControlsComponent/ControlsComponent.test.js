import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import ControlsComponent from './ControlsComponent';

describe('<ControlsComponent />', () => {
  test('it should mount', () => {
    render(<ControlsComponent />);
    
    const controlsComponent = screen.getByTestId('ControlsComponent');

    expect(controlsComponent).toBeInTheDocument();
  });
});