import React, { lazy, Suspense } from 'react';

const LazyControlsComponent = lazy(() => import('./ControlsComponent'));

const ControlsComponent = props => (
  <Suspense fallback={null}>
    <LazyControlsComponent {...props} />
  </Suspense>
);

export default ControlsComponent;
