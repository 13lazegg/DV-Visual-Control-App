import React from 'react';
import styles from './ControlsComponent.module.css';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Card from '@material-ui/core/Card';
import { Grid } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import Slider from '@material-ui/core/Slider';
import VolumeDown from '@material-ui/icons/VolumeDown';
import VolumeUp from '@material-ui/icons/VolumeUp';
import BrightnessLowIcon from '@material-ui/icons/BrightnessLow';
import BrightnessHighIcon from '@material-ui/icons/BrightnessHigh';
import Brightness2Icon from '@material-ui/icons/Brightness2';
import Brightness1Icon from '@material-ui/icons/Brightness1';
import { useSelector, useDispatch } from 'react-redux';
import {
  selectVideo,
  setVideo,
  data,
  selectedVolume,
  setVolume,
  togglePlay,
  play,
  brightness,
  setBrightness,
  contrast,
  setContrast
} from '../../reducers/reducer';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
    maxWidth: '100%'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  root: {
    display: 'flex',
    width: '100%'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 100,
    objectFit: 'contain'
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    minWidth: 300
  },
  controlsSlider: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    minWidth: 300
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

function ControlsComponent() {
  const classes = useStyles();
  const selected = useSelector(selectVideo);
  const selectedVol = useSelector(selectedVolume);
  const dataApi = useSelector(data);
  const isPlay = useSelector(play);
  const dispatch = useDispatch();
  const selectedBrightness = useSelector(brightness);
  const selectedcontrast = useSelector(contrast);
  let items = [];
  if (dataApi) {
    dataApi.map((element) => {
      return items = [...element.videos];
    })
  }
  const render = items.map((i, index) => (
    <MenuItem key={index} value={i}>
      <Card className={classes.root}>
        <CardMedia
          className={classes.cover}
          component="img"
          alt={i.title}
          src={i.thumb}
          title={i.title}
        />
        <div className={classes.details}>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
              {i.title}
            </Typography>
            <Typography variant="subtitle1" color="textSecondary" className={styles.Truncate}>
              {i.subtitle && i.subtitle.length > 10 ? i.subtitle.substring(0, 10) + "..." : i.subtitle}
            </Typography>
            <Tooltip title={i.description}>
              <Typography variant="subtitle2" color="textSecondary" className={styles.Truncate}>
                {i.description && i.description.length > 70 ? i.description.substring(0, 70) + "..." : i.description}
              </Typography>
            </Tooltip>
          </CardContent>
        </div>
      </Card>
    </MenuItem>
  ))
  const handleChange = (event) => {
    dispatch(setVideo(event.target.value))
  };

  const changeVol = (event, newValue) => {
    dispatch(setVolume(newValue))
  }

  const changeBri = (event, newValue) => {
    dispatch(setBrightness(newValue))
  }

  const changeCons = (event, newValue) => {
    dispatch(setContrast(newValue))
  }

  const toggle = () => {
    dispatch(togglePlay())
  }

  let button;
  if (isPlay) {
    button = <PauseIcon className={classes.playIcon} />
  } else {
    button = <PlayArrowIcon className={classes.playIcon} />
  }

  return (
    <Grid item container xs={12} direction="row" justify="space-between" alignItems="center" className={styles.ControlsComponent} data-testid="ControlsComponent">
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Seleccione su película</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={selected}
          onChange={handleChange}
        >
          {render}
        </Select>
      </FormControl>
      <div className={classes.controls}>
        <IconButton onClick={toggle} aria-label="play/pause">
          {button}
        </IconButton>
        <div className={classes.controlsSlider}>
          <Typography id="continuous-slider" gutterBottom>
            Volumen
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <VolumeDown />
            </Grid>
            <Grid item xs>
              <Slider value={selectedVol} step={0.1} min={0} max={1} onChange={changeVol} aria-labelledby="continuous-slider" />
            </Grid>
            <Grid item>
              <VolumeUp />
            </Grid>
          </Grid>
          <Typography id="continuous-slider" gutterBottom>
            Brillo
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <BrightnessLowIcon />
            </Grid>
            <Grid item xs>
              <Slider value={selectedBrightness} step={1} min={0} max={100} onChange={changeBri} aria-labelledby="continuous-slider" />
            </Grid>
            <Grid item>
              <BrightnessHighIcon />
            </Grid>
          </Grid>
          <Typography id="continuous-slider" gutterBottom>
            Contraste
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <Brightness2Icon />
            </Grid>
            <Grid item xs>
              <Slider value={selectedcontrast} step={1} min={0} max={100} onChange={changeCons} aria-labelledby="continuous-slider" />
            </Grid>
            <Grid item>
              <Brightness1Icon />
            </Grid>
          </Grid>
        </div>
      </div>
    </Grid>
  )
};

export default ControlsComponent;
