import { createSlice } from '@reduxjs/toolkit';

export const controlSlice = createSlice({
    name: 'control',
    initialState: {
        play: false,
        brightness: 100,
        contrast: 100,
        volume: 0.5,
        video: '',
        data: []
    },
    reducers: {
        togglePlay: state => {
            if(state.video !== ''){
                state.play = !state.play;
            }
        },
        setVolume: (state, action) => {
            state.volume = action.payload;
        },
        setBrightness: (state, action) => {
            state.brightness = action.payload;
        },
        setContrast: (state, action) => {
            state.contrast = action.payload;
        },
        setVideo: (state, action) => {
            state.video = action.payload;
        },
        setData: (state, action) => {
            state.data = action.payload;
        }
    },
});

export const { togglePlay, setVolume, setVideo, setData, setBrightness, setContrast } = controlSlice.actions;

export const selectVideo = state => state.control.video;

export const selectedVolume = state => state.control.volume;

export const brightness = state => state.control.brightness;

export const contrast = state => state.control.contrast;

export const play = state => state.control.play;

export const data = state => state.control.data;

export default controlSlice.reducer;