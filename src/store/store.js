import { configureStore } from '@reduxjs/toolkit';
import controlReducer from '../reducers/reducer';

export default configureStore({
    reducer: {
        control: controlReducer,
    },
});